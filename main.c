#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <zconf.h>


//ett program snurrar tills strömmen bryts
//kod som simulerar blinkande diod
//skriv ut SOS medelst morse
//lägg upp på git
int main()
{
    char input;
    int i = 0;

    system("cls");
    printf("EMBEDDED SYSTEMS SOS BEACON SIMULATOR\n"
                   "EARLY ACCESS ALPHA v0.1\n"
                   "\n"
                   "PRESS <ENTER> TO ABORT SIMULATION (Once it starts)\n\n");
    sleep(10);


    while( !_kbhit() && input != '\b' )
    {
        system("cls");

        if ((i>0 && i<=3) || (i>6 && i<=9))
        {
            printf(" . \n\n\n",i);
            sleep(1);
            system("cls");
            printf("\n\n\n");
            sleep(1);
        }
        else if (i>3 && i<=6)
        {
            printf(" - \n\n\n",i);
            sleep(1);
            system("cls");
            printf("\n\n\n");
            sleep(1);
        }
        else if(i>9 && i<15)
        {
            printf(" \n\n\n",i);
            sleep(1);
        }
        else
        {
            i = 0;
        }

        printf("\n\n\n");


        i++;
    }

//    while(1)
//    {
//        ;
//
//
//
//        input = getchar();
//
//        //while( !_kbhit() && userInput != '\b' )
////        if(getchar() == '0')
////        {
////            printf("EMERGENCYSHUTDOWN! JETISON THE CORE!\n");
////            break;
////        }
//    }
    printf("END\n");
}
